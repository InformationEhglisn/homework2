import java.util.Scanner;
public class Main {

    public static void main(String args[]) {
        int zarplatavchas = 0;
        while (zarplatavchas < 8) {
            System.out.print("Ваша зарплата в час: ");
            Scanner scan = new Scanner(System.in);
            zarplatavchas = scan.nextInt();
            if (zarplatavchas < 8) {
                System.out.println("Ваша зарплата в час не может быть меньше 8$");
            }
        }
        int chasov = 100;
        while (chasov > 60) {
            System.out.print("Кол-во проработанных часов: ");
            Scanner scan = new Scanner(System.in);
            chasov = scan.nextInt();
            if (chasov > 60) {
                System.out.println("Вы не можете работать больше 60 часов в неделю");
            }
        }
        double zchasov;
        if (chasov > 40) {
            zchasov = 40 + (chasov - 40) * 1.5;
        } else zchasov = chasov;
        double zarplata = zarplatavchas * zchasov;
        System.out.println("Ваша зарплата составит:" + zarplata + " $");
    }

}
